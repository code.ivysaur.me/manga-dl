# manga-dl

![](https://img.shields.io/badge/written%20in-PHP-blue)

A command-line batch downloader for manga websites.

It features automatic .cbz packing, and full gallery or individual chapter download (depending on backend).

Tags: anime

## Supported Sites

Some sites were added at user request.

- MangaBird.com
- MangaInn.com
- MangaFox.me (individual entries and galleries)
- MangaReader.net
- MangaTraders.com
- MangaDoom.com
- MangaStream.to
- Batoto.net (Bato.to)
- Hitomi.la
- GEH
- NH

## Usage


```
Usage:
   manga-dl [options]

Options:
   {any URL}             Add URL for attempted download
   --dry-run             Scan URLs, no download
   --no-zip              Don't pack downloaded files into a .cbz archive
   --usage, --help       Display this message
   --zip                 Pack downloaded files into a .cbz archive (default)
```


## Changelog

2017-02-04: v15
- Compatibility updates for Mangafox and NH
- [⬇️ manga-dl-v15.tar.xz](dist-archive/manga-dl-v15.tar.xz) *(5.26 KiB)*


2016-09-18: v14
- Compatibility updates for Mangafox, Hitomi and NH
- Dsplay warning when downloading a zero-byte file
- [⬇️ manga-dl-v14.tar.xz](dist-archive/manga-dl-v14.tar.xz) *(5.13 KiB)*


2015-12-02: v13
- MangaFox: Support downloading all chapters in gallery
- MangaFox: Fix partial chapters (e.g. c23.5)
- Zip by default, use `--no-zip` to disable
- [⬇️ manga-dl-v13.tar.xz](dist-archive/manga-dl-v13.tar.xz) *(5.01 KiB)*


2015-12-01: v12
- Improve support for Hitomi.la site
- [⬇️ manga-dl-v12.tar.xz](dist-archive/manga-dl-v12.tar.xz) *(4.45 KiB)*


2015-12-01: v11
- Feature: Support Hitomi.la site
- [⬇️ manga-dl-v11.tar.xz](dist-archive/manga-dl-v11.tar.xz) *(4.45 KiB)*


2015-11-29: v10
- Compatibility updates for Mangafox, Batoto, and NH
- Allow overwriting zero-byte files
- Fix encoded characters appearing in titles (NH)
- [⬇️ manga-dl-v10.tar.xz](dist-archive/manga-dl-v10.tar.xz) *(4.25 KiB)*


2015-10-11: v9
- Feature: Add `--zip` as second argument to produce .cbz file
- [⬇️ manga-dl-v9.7z](dist-archive/manga-dl-v9.7z) *(4.01 KiB)*


2015-10-11: v8
- Feature: Support NH site
- Feature: Support bato.to URLs
- Enhancement: Preserve session cookies
- Enhancement: Preserve non-latin alphanumeric characters in filenames
- Enhancement: Display error message on network failure
- [⬇️ manga-dl-v8.7z](dist-archive/manga-dl-v8.7z) *(3.41 KiB)*


2014-05-31: v7
- Added batoto.net support
- Spoof user agent
- Fix issue with compressed body responses
- [⬇️ manga-dl-v7.zip](dist-archive/manga-dl-v7.zip) *(3.21 KiB)*


2013-09-21: v5
- Initial public release
- [⬇️ manga-dl-v5.zip](dist-archive/manga-dl-v5.zip) *(11.54 KiB)*


2013-08-18: v0
- Combined distribution of earlier entries
- [⬇️ manga-dl-v0.7z](dist-archive/manga-dl-v0.7z) *(1.67 KiB)*


2013-08-18: v0-mangasadpanda
- Feature: Support GEH site

2013-06-03: v0-mangainn
- Feature: Support MangaInn site

2013-05-26: v0-mangatraders
- Feature: Support MangaTraders site

2013-05-03: v0-mangafox
- Feature: Support MangaFox site

2013-04-02: v0-mangabird
- Feature: Support MangaBird site

2013-03-17: v0-mangareader
- Initial private release
- Feature: Support MangaReader site
